find_package(PkgConfig)
pkg_check_modules(PC_LIBEXTRA libextra)
set(LIBEXTRA_DEFINITIONS ${PC_LIBEXTRA_CFLAGS_OTHER})

find_path(EXTRA_INCLUDE_DIR extra/socket.h
          HINTS ${PC_LIBEXTRA_INCLUDEDIR} ${PC_LIBEXTRA_INCLUDE_DIRS})

set(Extra_INCLUDE_DIRS ${EXTRA_INCLUDE_DIR})

find_library(LIBSOCKET_LIBRARY NAMES socket libsocket
             HINTS ${PC_LIBEXTRA_LIBDIR} ${PC_LIBEXTRA_LIBRARY_DIRS})
find_library(LIBANSI_LIBRARY NAMES ansi libansi
             HINTS ${PC_LIBEXTRA_LIBDIR} ${PC_LIBEXTRA_LIBRARY_DIRS})

set(Extra_LIBRARIES ${LIBSOCKET_LIBRARY} ${LIBANSI_LIBRARY})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(LibExtra DEFAULT_MSG Extra_LIBRARIES Extra_INCLUDE_DIRS)
