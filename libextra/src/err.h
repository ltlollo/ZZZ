#ifndef EXTRA_ERR_H
#define EXTRA_ERR_H


#include <stdexcept>
#include <string>
#include <errno.h>
#include "func.h"

namespace err {

template<bool with_errno, typename T>
[[noreturn]] inline std::enable_if_t<!with_errno> dbg(T&& msg) {
    throw std::runtime_error(std::forward<T>(msg));
}

template<bool with_errno, typename T>
[[noreturn]]inline std::enable_if_t<with_errno> dbg(T&& msg) {
    auto err = errno;
    throw std::runtime_error(std::string(std::forward<T>(msg)) + "; errno: " +
                             std::to_string(err));
}

template<bool with_errno, typename T, typename... Ts>
inline void dbgfail(T&& msg, Ts&&... args) {
    if (fun::fn(std::forward<Ts>(args)...) < 0) {
        dbg<with_errno>(std::forward<T>(msg));
    }
}

template<bool with_errno, typename T, typename U>
inline std::enable_if_t<std::is_integral<U>::value> dbgfail(T&& msg, U i) {
    if (i < 0) {
        dbg<with_errno>(std::forward<T>(msg));
    }
}

template<bool with_errno, typename T, typename... Ts>
inline void dbgreturn(T&& msg, Ts&&... args) {
    if (fun::fn(std::forward<Ts>(args)...)) {
        dbg<with_errno>(std::forward<T>(msg));
    }
}

template<bool with_errno, typename T, typename U>
inline std::enable_if_t<std::is_integral<U>::value> dbgreturn(T&& msg, U i) {
    if (i) {
        dbg<with_errno>(std::forward<T>(msg));
    }
}

template<typename... Ts>
inline void donotfail(Ts&&... args) {
    dbgfail<false>(std::forward<Ts>(args)...);
}

template<typename... Ts>
inline void doreturn(Ts&&... args) {
    dbgreturn<false>(std::forward<Ts>(args)...);
}

template<typename... Ts>
inline void donotfail_errno(Ts&&... args) {
    dbgfail<true>(std::forward<Ts>(args)...);
}

template<typename... Ts>
inline void doreturn_errno(Ts&&... args) {
    dbgreturn<true>(std::forward<Ts>(args)...);
}

}

#endif // EXTRA_ERR_H
