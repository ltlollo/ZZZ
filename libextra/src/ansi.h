#ifndef EXTRA_ANSI_H
#define EXTRA_ANSI_H

#include "func.h"
#include "proto.h"
#include "err.h"

#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include <functional>

namespace ansi {

using ::sockaddr_un;
using ::recv;
using ::strcpy;
using ::unlink;
using ::listen;
using ::socket;
using ::close;
using ::get_current_dir_name;
using ::fork;
using ::getopt;
using ::optarg;
using ::optind;
using ::exit;
using ::ucred;

constexpr unsigned max_conn{5};
constexpr int ss{SOL_SOCKET}, unix{AF_UNIX}, stream{SOCK_STREAM},
stdin{STDIN_FILENO}, nosig{MSG_NOSIGNAL}, stdout{STDOUT_FILENO},
stderr{STDERR_FILENO}, sigint{SIGINT}, sigpipe{SIGPIPE},
scm_cred{SCM_CREDENTIALS};

namespace so {
    constexpr int cred{SO_PASSCRED}, time{SO_RCVTIMEO}, rlow{SO_RCVLOWAT};
}   // socket options

int sun_connect(const int fd, const sockaddr_un* addr) noexcept;

int sun_bind(const int fd, const sockaddr_un* addr) noexcept;

int provide_creds(const int fd) noexcept;

int signal(const int sig, void(*f)(int, siginfo_t*, void*)) noexcept;

int cred_recv(const int fd, char* buf, const size_t size, ucred* cred) noexcept;

int cred_send(const int fd, char* buf, const size_t len) noexcept;

bool eq(const ucred& fst, const ucred& snd) noexcept;

namespace exp {
template<typename F, typename... Args> bool dfork(F&& f, Args&&... args) {
        pid_t fd{ansi::fork()};
        err::donotfail_errno("fork once", fd);
        if (fd) { return false; }
        fd = ansi::fork();
        err::donotfail_errno("fork twice", fd);
        if (fd) { return false; }
        f(std::forward<Args>(args)...);
        return true;
}

bool dfork();

} // namespace exp

} // ansi namespace

#endif // EXTRA_ANSI_H
