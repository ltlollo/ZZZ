#ifndef FNTRAITS_H
#define FNTRAITS_H

#include "futils.h"

template<typename Ret, typename... Args> using fn_t = Ret(*)(Args...);

template<typename T>
struct Function : public Function<decltype(&T::operator())>{
};
template<typename T, typename Ret, typename... Args>
struct Function<Ret(T::*)(Args...)> {
    using return_t = Ret;
    using ptr_t = fn_t<Ret, Args...>;
    using args_t = Pack<Args...>;
};
template<typename T, typename Ret, typename... Args>
struct Function<Ret(T::*)(Args...) const> {
    using return_t = Ret;
    using ptr_t = fn_t<Ret, Args...>;
    using args_t = Pack<Args...>;
};
template<typename T, typename Ret, typename... Args>
struct Function<Ret(T::*)(Args...) volatile> {
    using return_t = Ret;
    using ptr_t = fn_t<Ret, Args...>;
    using args_t = Pack<Args...>;
};
template<typename Ret, typename... Args>
struct Function<Ret(*)(Args...)> {
    using return_t = Ret;
    using ptr_t = fn_t<Ret, Args...>;
    using args_t = Pack<Args...>;
};

namespace {
struct True { static constexpr bool value = true; };
struct False { static constexpr bool value = false; };
}

template<typename... T> struct is_function : False {};
template<typename T, typename Ret, typename... Args>
struct is_function<Ret(T::*)(Args...)> : True {};
template<typename T, typename Ret, typename... Args>
struct is_function<Ret(T::*)(Args...) const> : True {};
template<typename T, typename Ret, typename... Args>
struct is_function<Ret(T::*)(Args...) volatile> : True {};
template<typename Ret, typename... Args>
struct is_function<Ret(*)(Args...)> : True {};

template<typename C> struct is_functor {
    struct Ty { template<typename T> static bool has(T f); };
    template<typename T> static True ftor(decltype(Ty::has(&T::operator())));
    template<typename> static False ftor(...);
    static constexpr bool value = std::is_same<decltype(ftor<C>(true)), True>();
};

template<typename T> struct is_callable {
    static constexpr bool value = is_function<T>::value
        || is_functor<T>::value;
    constexpr bool operator()() {
        return value;
    }
};

template<typename... T> struct MakeFn;
template<typename R, typename... T> struct MakeFn<R, Pack<T...>> {
    static constexpr auto get() noexcept {
        return [](T...) -> R {};
    }
};

template<typename F> using ret_of_t = typename Function<F>::return_t;
template<typename F> using args_of_t = typename Function<F>::args_t;

#endif // FNTRAITS_H
