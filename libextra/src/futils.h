#ifndef FUTILS_H
#define FUTILS_H


template<typename T> using t_of = typename T::type;
template<typename T> constexpr bool v_of(...) { return T::value; }

template<typename... Ts> struct Pack {
    static constexpr size_t size = sizeof...(Ts);
};

template<size_t N, typename T> struct Unpack;
template<size_t N, typename T, typename... Ts>
struct Unpack<N, Pack<T, Ts...>> {
    using type = t_of<Unpack<N-1, Pack<Ts...>>>;
};
template<typename T, typename... Ts>
struct Unpack<0, Pack<T, Ts...>>{
    using type = T;
};

#endif // FUTILS_H
