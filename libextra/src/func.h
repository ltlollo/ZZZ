#ifndef EXTRA_FUNC_H
#define EXTRA_FUNC_H

#include <utility>

namespace fun {

template<typename P, typename... Ts>
[[noreturn]] inline void loop(P&& f, Ts&&... args) {
    while(true) {
        f(std::forward<Ts>(args)...);
    }
}

template<typename P, typename F, typename... Ts>
inline void until(P&& cont, F f, Ts&&... args) {
    while(cont()) {
        f(std::forward<Ts>(args)...);
    }
}

template<typename P, typename... Ts>
inline constexpr auto fn(P&& f, Ts&&... args) {
    return f(std::forward<Ts>(args)...);
}

template<typename E>
struct Catch {
    template<typename T, typename U, typename... UU>
    void operator()(T&& err, U&& f, UU... Us) {
        try {
            f(std::forward<UU>(Us)...);
        } catch(E e) {
            err(e);
        }
    }
};

}

#endif // EXTRA_FUNC_H
