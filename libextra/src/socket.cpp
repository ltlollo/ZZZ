#include "socket.h"

using namespace fun;
using namespace msg;
using namespace ansi;
using namespace data;
using std::swap;

socklen_t socklen{sizeof(sockaddr_un)};

namespace file {

Socket Socket::clone() const {
    return Socket(*this);
}

void Socket::listen(unsigned max) const {
    err::donotfail_errno("listen", ansi::listen, m_fd, max);
}

void Socket::addr(const sockaddr_un& addr) {
    m_addr = addr;
}

Socket& Socket::time(unsigned sec, unsigned usec)  {
   timeval t{sec, usec};
   err::donotfail_errno("time", setsockopt, m_fd, ss, so::time, &t,
                        sizeof(timeval));
   return *this;
}

Socket::Socket() : m_fd{ansi::socket(unix, stream, 0)},
    m_addr{unix, {0}} {
    err::donotfail_errno("unix", m_fd);
}

Socket::~Socket() noexcept {
    if (m_fd > 0) {
        ansi::close(m_fd);
    }
}

Socket::Socket(const Socket& rhs)
    : m_fd{accept(rhs.m_fd, (sockaddr*)&m_addr, &socklen)} {
    err::donotfail_errno("accept", m_fd);
}


Socket::Socket(Socket&& rhs) : m_fd{rhs.m_fd} {
    rhs.m_fd = -1;
}

int Socket::fd() const noexcept {
    return m_fd;
}

Socket& Socket::operator=(const Socket& rhs) {
    // self assign is a non-blocking move
    if (&rhs != this) {
        this->close();
        m_fd = accept(rhs.m_fd, (sockaddr*)&m_addr, &socklen);
        err::donotfail_errno("accept", m_fd);
    }
    return *this;
}

Socket& Socket::operator=(Socket&& rhs) {
    if(&rhs != this) {
        this->close();
        swap(m_fd, rhs.m_fd);
        swap(m_addr, rhs.m_addr);
    }
    return *this;
}

void Socket::close() {
    if (m_fd < 0) {
        return;
    }
    int res{ansi::close(m_fd)};
    m_fd = -1;
    err::doreturn_errno("close", res);
}

void Socket::operator>>(std::string& s) {
    s += Packet<msg::msglen, std::string>().rcv(this->fd());
}

void CredRecv::operator>>(WithCred<std::string>& s) {
    auto res = Packet<msg::msglen, std::string>().rcv_cred(m_s.fd());
    err::doreturn("cred mismatch", !s.eq_cred(res));
    s.data += res.data;
}

template<>
const Socket& Socket::send(const std::string& msg) {
    auto it = std::begin(msg);
    for (size_t i{0}; i < msg.size()/msg::msglen; ++i, it += msg::msglen) {
        *this << std::string(it, it+msg::msglen);
    } if (msg.size() % msg::msglen) {
        *this << std::string(it, std::end(msg));
    }
    return *this;
}

void Socket::provide_cred() {
    err::donotfail_errno("cred", ansi::provide_creds, m_fd);
}

void Socket::connect(const std::string& path) {
    err::doreturn("socket path too big", path.size() > 107);
    m_addr = ansi::sockaddr_un{ansi::unix, {0}};
    ansi::strcpy(m_addr.sun_path, path.c_str());
    err::donotfail_errno("connect", ansi::sun_connect, m_fd, &m_addr);
}

Bind::Bind(const std::string& path, Socket& s) : m_addr{ansi::unix, {0}} {
    err::doreturn("socket path too big", path.size() > 107);
    ansi::strcpy(m_addr.sun_path, path.c_str());
    err::donotfail_errno("bind", ansi::sun_bind, s.fd(), &m_addr);
    s.addr(m_addr);
}

} // file namespace
