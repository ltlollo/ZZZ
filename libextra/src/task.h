#ifndef EXTRA_TASK_H
#define EXTRA_TASK_H

#include <vector>
#include <tuple>
#include <pthread.h>
#include "fntraits.h"

namespace task {

enum Status { Running, Ok, ECreate, EJoin, ECancel };

template<size_t... Ns> struct Seq{};
template<size_t N, size_t... Ns> struct GenSeq : GenSeq<N-1, N-1, Ns...>{};
template<size_t... Ns> struct GenSeq<0, Ns...>{ using type = Seq<Ns...>; };
template<size_t N> using make_seq_t = typename GenSeq<N>::type;

template<unsigned N> using Threads = std::make_integer_sequence<unsigned, N>;

template<typename T, size_t... Ns> constexpr auto caller(T* t) {
    t->result = t->call(Seq<Ns...>{});
    t->err = Ok;
}

template<typename T, size_t... Ns> constexpr void vcaller(T* t) {
    t->call(Seq<Ns...>{});
    t->err = Ok;
}

template<typename Ret, typename Fun, typename... Args>
struct FunStore {
    Fun f;
    std::tuple<Args...> args;
    pthread_t t;
    Ret result;
    Status err{Ok};

    FunStore(Fun&& f, std::tuple<Args...>&& args) :
        f{std::forward<Fun>(f)},
        args{std::forward<std::tuple<Args...>
             >(args)} {
    }
    Ret operator()() {
        return call(make_seq_t<sizeof...(Args)>{});
    }
    template<size_t... Ns> Ret call(Seq<Ns...>) {
        return f(std::get<Ns>(args)...);
    }
    template<size_t... Ns> void call_async(Seq<Ns...>) {
        err = Running;
        if (pthread_create(&t, nullptr,
                           (fn_t<void*, void*>)caller<
                           FunStore<Ret, Fun, Args...>, Ns...>, (void*)this)) {
            err = ECreate;
        }
    }
    void async() {
        return call_async(make_seq_t<sizeof...(Args)>{});
    }
    void join() {
        if ( err == Ok || err == Running) {
            if (pthread_join(t, nullptr)) {
                err = EJoin;
            }
        }
    }
    void cancel() {
        if ( err == Ok || err == Running) {
            if (pthread_cancel(t)) {
                err = ECancel;
            }
        }
    }
};

template<typename Fun, typename... Args>
struct FunStore<void, Fun, Args...> {
    Fun f;
    std::tuple<Args...> args;
    pthread_t t;
    Status err{Ok};

    FunStore(Fun&& f, std::tuple<Args...>&& args) :
        f{std::forward<Fun>(f)},
        args{std::forward<std::tuple<Args...>
             >(args)} {
    }
    void operator()() {
        call(make_seq_t<sizeof...(Args)>{});
    }
    template<size_t... Ns> void call(Seq<Ns...>) {
        f(std::get<Ns>(args)...);
    }
    template<size_t... Ns> void call_async(Seq<Ns...>) {
        err = Running;
        if (pthread_create(&t, nullptr,
                          (fn_t<void*, void*>)vcaller<
                          FunStore<void, Fun, Args...>, Ns...>,
                          (void*)this)) {
            err = ECreate;
        }
    }
    void async() {
        return call_async(make_seq_t<sizeof...(Args)>{});
    }
    void join() {
        if ( err == Ok || err == Running) {
            if (pthread_join(t, nullptr)) {
                err = EJoin;
            }
        }
    }
    void cancel() {
        if ( err == Ok || err == Running) {
            if (pthread_cancel(t)) {
                err = ECancel;
            }
        }
    }
};

template<typename Fun, typename... Args>
const constexpr auto make_function(Fun f, Args&&... args) {
    using fun_t = typename Function<decltype(f)>::ptr_t;
    using ret_t = typename Function<decltype(f)>::return_t;
    return FunStore<ret_t, fun_t, Args...
            >((fun_t)f, std::tuple<Args...>{std::forward<Args>(args)...});
}

struct ForEachRef {
    template<typename F, typename T, typename... TT>
    ForEachRef(F&& f, T& t, TT&... tt) {
        f(t);
        ForEachRef(std::forward<F>(f), tt...);
    }
    template<typename F, typename T> ForEachRef(F&& f, T& t) { f(t); }
};

struct ForEach {
    template<typename F, typename T, typename... TT>
    ForEach(F&& f, T&& t, TT&&... tt) {
        f(std::forward<T>(t));
        ForEach(std::forward<F>(f), std::forward<TT>(tt)...);
    }
    template<typename F, typename T> ForEach(F&& f, T&& t) {
        f(std::forward<T>(t));
    }
};

template<typename T, typename F, unsigned... Ns>
auto apply(F&& f, T&& res, std::integer_sequence<unsigned, Ns...>) {
    task::ForEachRef(f, std::get<Ns>(res)...);
}

template<typename T, typename Fun, typename Fil>
auto split(const std::vector<T>& vec, Fun fun, Fil filter, unsigned Nth,
           unsigned N) {
    auto range_beg = vec.size()*(Nth)/N;
    auto range_end = vec.size()*(Nth+1)/N;
    auto len = range_end - range_beg;
    std::vector<std::result_of_t<Fun(T)>> res;
    if (!len) {
        return res;
    }
    res.reserve(len);
    for (size_t i = range_beg; i < range_end; ++i) {
        if (filter(vec[i])) {
            res.emplace_back(fun(vec[i]));
        }
    }
    res.shrink_to_fit();
    return res;
}

template<typename T, typename Fun, typename Fil, unsigned... Ns>
auto compute(const std::vector<T>& vec, Fun fun, Fil filter,
             std::integer_sequence<unsigned, Ns...>) {
    auto f = [](const std::vector<T>& vec, Fun fun, Fil filter, unsigned nth,
             unsigned of) {
        return split(vec, fun, filter, nth, of);
    };
    auto res = std::make_tuple(make_function(f, vec, fun, filter, Ns,
                                             sizeof...(Ns))...);
    ForEachRef([](auto& t){ t.async(); }, std::get<Ns>(res)...);
    ForEachRef([](auto& t){ t.join(); }, std::get<Ns>(res)...);
    ForEachRef([](auto& t){
        if (t.err != Status::Ok) throw std::runtime_error("task failure");
    }, std::get<Ns>(res)...);
    return res;
}

template<typename T, typename Fun, typename Fil, typename Red, unsigned... Ns>
auto map_reduce(const std::vector<T>& vec, Fun fun, Fil filter, Red red,
             std::integer_sequence<unsigned, Ns...> th) {
    apply(std::forward<Red>(red),
          task::compute(vec, std::forward<Fun>(fun),
                         std::forward<Fil>(filter), th),
          th);
}

template<typename T, typename Fun, typename Fil, unsigned... Ns>
auto collect(const std::vector<T>& vec, Fun&& fun, Fil&& fil,
             std::integer_sequence<unsigned, Ns...> th) {
    std::vector<decltype(fun(vec[0]))> res;
    map_reduce(vec, fun, fil, [&](auto& it){
        res.insert(end(res),
        make_move_iterator(begin(it.result)),
        make_move_iterator(end(it.result))
    );}, th);
    return res;
}

/* indexed variant */

template<typename T, typename Fun, typename Fil>
auto split_indexed(const std::vector<T>& vec, Fun fun, Fil filter, unsigned Nth,
           unsigned N) {
    auto range_beg = vec.size()*(Nth)/N;
    auto range_end = vec.size()*(Nth+1)/N;
    auto len = range_end - range_beg;
    std::vector<std::result_of_t<Fun(T, size_t&)>> res;
    if (!len) {
        return res;
    }
    res.reserve(len);
    for (size_t i = range_beg; i < range_end; ++i) {
        if (filter(vec[i], i)) {
            res.emplace_back(fun(vec[i], i));
        }
    }
    res.shrink_to_fit();
    return res;
}

template<typename T, typename Fun, typename Fil, unsigned... Ns>
auto compute_indexed(const std::vector<T>& vec, Fun fun, Fil filter,
             std::integer_sequence<unsigned, Ns...>) {
    auto f = [](const std::vector<T>& vec, Fun fun, Fil filter, unsigned nth,
             unsigned of) {
        return split_indexed(vec, fun, filter, nth, of);
    };
    auto res = std::make_tuple(make_function(f, vec, fun, filter, Ns,
                                             sizeof...(Ns))...);
    ForEachRef([](auto& t){ t.async(); }, std::get<Ns>(res)...);
    ForEachRef([](auto& t){ t.join(); }, std::get<Ns>(res)...);
    ForEachRef([](auto& t){
        if (t.err != Status::Ok) throw std::runtime_error("task failure");
    }, std::get<Ns>(res)...);
    return res;
}

template<typename T, typename Fun, typename Fil, typename Red, unsigned... Ns>
auto map_reduce_indexed(const std::vector<T>& vec, Fun fun, Fil filter, Red red,
             std::integer_sequence<unsigned, Ns...> th) {
    apply(std::forward<Red>(red),
          task::compute_indexed(vec, std::forward<Fun>(fun),
                         std::forward<Fil>(filter),th),
          th);
}

template<typename T, typename Fun, typename Fil, unsigned... Ns>
auto collect_indexed(const std::vector<T>& vec, Fun&& fun, Fil&& fil,
             std::integer_sequence<unsigned, Ns...> th) {
    std::vector<decltype(fun(vec[0], 0))> res;
    map_reduce_indexed(vec, fun, fil, [&](auto& it){
        res.insert(end(res),
        make_move_iterator(begin(it.result)),
        make_move_iterator(end(it.result))
    );}, th);
    return res;
}

/* unfiltered variant */

template<typename T, typename Fun>
auto split(const std::vector<T>& vec, Fun fun, unsigned Nth,
           unsigned N) {
    auto range_beg = vec.size()*(Nth)/N;
    auto range_end = vec.size()*(Nth+1)/N;
    auto len = range_end - range_beg;
    std::vector<std::result_of_t<Fun(T)>> res;
    if (!len) {
        return res;
    }
    res.reserve(len);
    for (size_t i = range_beg; i < range_end; ++i) {
        res.emplace_back(fun(vec[i]));
    }
    return res;
}

template<typename T, typename Fun, unsigned... Ns>
auto compute(const std::vector<T>& vec, Fun fun,
             std::integer_sequence<unsigned, Ns...>) {
    auto f = [](const std::vector<T>& vec, Fun fun, unsigned nth,
             unsigned of) {
        return split(vec, fun, nth, of);
    };
    auto res = std::make_tuple(make_function(f, vec, fun, Ns,
                                             sizeof...(Ns))...);
    ForEachRef([](auto& t){ t.async(); }, std::get<Ns>(res)...);
    ForEachRef([](auto& t){ t.join(); }, std::get<Ns>(res)...);
    ForEachRef([](auto& t){
        if (t.err != Status::Ok) throw std::runtime_error("task failure");
    }, std::get<Ns>(res)...);
    return res;
}

template<typename T, typename Fun, typename Red, unsigned... Ns>
auto map_reduce(const std::vector<T>& vec, Fun&& fun, Red&& red,
             std::integer_sequence<unsigned, Ns...> th) {
    apply(std::forward<Red>(red),
          task::compute(vec, std::forward<Fun>(fun), th),
          th);
}

template<typename T, typename Fun, unsigned... Ns>
auto collect(const std::vector<T>& vec, Fun&& fun,
             std::integer_sequence<unsigned, Ns...> th) {
    std::vector<decltype(fun(vec[0]))> res;
    map_reduce(vec, fun, [&](auto& it){
        res.insert(end(res),
        make_move_iterator(begin(it.result)),
        make_move_iterator(end(it.result))
    );}, th);
    return res;
}

template<unsigned N> struct Parallel {
    template<typename... Ts> static auto collect(Ts... args) {
        return task::collect(std::forward<Ts>(args)..., task::Threads<N>{});
    }
    template<typename... Ts> static auto collect_indexed(Ts... args) {
        return task::collect_indexed(std::forward<Ts>(args)...,
                                     task::Threads<N>{});
    }
};

} // end of namespace task

#endif // EXTRA_TASK_H
