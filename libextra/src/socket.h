#ifndef EXTRA_SOCKET_H
#define EXTRA_SOCKET_H

#include <string>
#include <vector>

#include "func.h"
#include "proto.h"
#include "ansi.h"
#include "data.h"

namespace file {

class Socket {
    int m_fd;
    ansi::sockaddr_un m_addr;

public:
    Socket();
    Socket(const Socket& rhs);
    Socket(Socket&& rhs);
    Socket& operator=(const Socket& rhs);
    Socket& operator=(Socket&& rhs);
    ~Socket() noexcept;
    Socket clone() const;
    void close();
    int fd() const noexcept;
    void addr(const ansi::sockaddr_un& addr);
    void operator>>(std::string& s);
    template<typename T> void operator>>(std::vector<T>& msg);
    template<typename T> void operator>>(T& msg);
    template<typename T> T recv(const size_t len);
    template<typename T> T recv();
    template<typename T> const Socket& send(const T& msg);
    template<typename T> const Socket& send(const std::vector<T>& msg);
    template<unsigned N> void connect(const char(&path)[N]);
    void connect(const std::string& str);
    void listen(unsigned max = ansi::max_conn) const;
    Socket& time(unsigned sec, unsigned usec = 0);
    void provide_cred();
};

template<typename T>
void Socket::operator>>(std::vector<T>& msg) {
    auto vec = data::Packet<msg::msglen, std::vector<T>>().rcv(this->fd());
    msg.insert(std::end(msg), std::make_move_iterator(std::begin(vec)),
               std::make_move_iterator(std::end(vec)));
}

template<typename T>
void Socket::operator>>(T& msg) {
    static_assert(sizeof(T) <= msg::maxlen, "max socksize exceded");
    static_assert(sizeof(T) > 0, "must be positive");
    err::donotfail_errno("recv", ansi::recv, m_fd, &msg, sizeof(T), 0);
}

template<typename T>
T Socket::recv(const size_t msgsize) {
    T res;
    if (!msgsize) {
        return res;
    }
    res.reserve(msgsize);
    for (size_t i{0}; i < msgsize/msg::msglen; ++i) {
        *this >> res;
    } if (msgsize % msg::msglen) {
        *this >> res;
        res.resize(msgsize);
    }
    return res;
}

template<typename T> T Socket::recv() {
    return data::Packet<msg::msglen, T>().rcv(this->fd());
}

template<typename T>
const Socket& Socket::send(const std::vector<T>& msg) {
    auto it = std::begin(msg);
    for (size_t i{0}; i < msg.size()/msg::msglen; ++i, it += msg::msglen) {
        *this << std::vector<T>(it, it+msg::msglen);
    } if (msg.size() % msg::msglen) {
        *this << std::vector<T>(it, std::end(msg));
    }
    return *this;
}

template<typename T>
const Socket& Socket::send(const T& msg) {
    return *this << msg;
}

template<unsigned N>
void Socket::connect(const char(&path)[N]) {
    static_assert(N <= 107, "socket path too big");
    m_addr = ansi::sockaddr_un{ansi::unix, {0}};
    ansi::strcpy(m_addr.sun_path, path);
    err::donotfail_errno("connect", ansi::sun_connect, m_fd, &m_addr);
}

template<typename T>
Socket& operator<<(Socket& s, const T& msg) {
    data::Packet<msg::msglen, T>().snd(s.fd(), msg);
    return s;
}

class Bind {
    ansi::sockaddr_un m_addr;
public:
    template<unsigned N> Bind(const char (&path)[N], Socket& s);
    Bind(const std::string& path, Socket& s);
    ~Bind() {
        ansi::unlink(m_addr.sun_path);
    }
    ansi::sockaddr_un addr() const noexcept { return m_addr; }
};

template<unsigned N>
Bind::Bind(const char (&path)[N], Socket& s) : m_addr{ansi::unix, {0}} {
    static_assert(N <= 107, "socket path too big");
    ansi::strcpy(m_addr.sun_path, path);
    err::donotfail_errno("bind", ansi::sun_bind, s.fd(), &m_addr);
    s.addr(m_addr);
}

class CredRecv {
    Socket& m_s;
public:
    constexpr CredRecv(Socket& s) : m_s{s} {}
    template<typename T> data::WithCred<T> recv_cred();
    template<typename T> data::WithCred<T> recv_cred_notnil(const size_t size);
    void operator>>(data::WithCred<std::string>& s);
    template<typename T> void operator>>(data::WithCred<T>& msg);
    template<typename T> void operator>>(data::WithCred<std::vector<T>>& msg);
};

template<typename T>
data::WithCred<T> CredRecv::recv_cred() {
    return data::Packet<msg::msglen, T>().rcv_cred(m_s.fd());
}

template<typename T>
 data::WithCred<T> CredRecv::recv_cred_notnil(const size_t size) {
    if (!size) {
        return data::Packet<0, msg::Empty<T>>().rcv_cred(m_s.fd());
    }
    auto res = data::Packet<msg::msglen, T>().rcv_cred(m_s.fd());
    if (size/msg::msglen < 1) {
        res.data.resize(size);
        return res;
    }
    for (size_t i{0}; i < size/msg::msglen - 1; ++i) {
        *this >> res;
    } if (size % msg::msglen) {
        *this >> res;
        res.data.resize(size);
    }
    return res;
}

template<typename T>
void CredRecv::operator>>(data::WithCred<std::vector<T>>& msg) {
    auto res = data::Packet<msg::msglen, std::vector<T>>().rcv_cred(m_s.fd());
    err::doreturn("cred mismatch ", !msg.eq_cred(res));
    msg.data.insert(std::end(msg.data),
                    std::make_move_iterator(std::begin(res.data)),
                    std::make_move_iterator(std::end(res.data)));
}

template<typename T>
void CredRecv::operator>>(data::WithCred<T>& msg) {
    auto res = data::Packet<msg::msglen, T>().rcv_cred(m_s.fd());
    err::doreturn("cred mismatch", !msg.eq_cred(res));
    std::swap(msg, res);
}

}

#endif // EXTRA_SOCKET_H
