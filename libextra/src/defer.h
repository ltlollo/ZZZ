#ifndef EXTRA_DEFER_H
#define EXTRA_DEFER_H

#include <tuple>

namespace scope {

template<typename... Ts> struct Owned : public std::tuple<Ts...> {
    using std::tuple<Ts...>::tuple;
};

template<> struct Owned<> {
};

template<typename F, typename Tp, size_t... Seq>
auto call(F&& f, Tp&& tp, std::index_sequence<Seq...>) {
    return f(std::get<Seq>(tp)...);
}

template<typename F, typename... Args>
class Defer {
    bool dirty{false};
    F f;
    Owned<Args...> args;
public:
    constexpr Defer(F&& f, Args&&... args)
        : f{std::forward<F>(f)}, args{std::forward<Args>(args)...} {
    }
    Defer(Defer&& other) : f{other.f} {
        args = other.args;
        other.dirty = true;
    }
    ~Defer() noexcept {
        if (!dirty) {
            call(f, args, std::index_sequence_for<Args...>());
        }
    }
};

template<typename... Ts>
constexpr auto defer(Ts&&... ts) {
    return Defer<Ts...>(std::forward<Ts>(ts)...);
}
}

#endif // EXTRA_DEFER_H

