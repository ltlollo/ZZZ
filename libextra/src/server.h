#ifndef EXTRA_SERVER_H
#define EXTRA_SERVER_H

#include "socket.h"
#include <thread>

namespace srv {

struct Clean{};

struct Server {
    std::string path;
    file::Socket sock;
    file::Bind socfile;

public:
    template<typename T> explicit Server(T&& path);
    template<typename T, typename... TT>
    void run(T&& job, TT&&... Ts);
};

template<typename T>
Server::Server(T&& path) : sock{}, socfile(path, sock) {
    err::donotfail_errno("sigaction", ansi::signal, ansi::sigint,
                         [&](int, siginfo_t*, void*){
        throw Clean();
    });
}

template<typename T, typename... TT>
void Server::run(T&& job, TT&&... Ts) {
    sock.listen();
    fun::loop([&]() {
        std::thread(std::forward<T>(job), sock.clone(), std::forward<TT>(Ts)...)
                .detach();
    });
}

} // end of namespace srv

#endif // EXTRA_SERVER_H
