#ifndef EXTRA_OPTIONAL_H
#define EXTRA_OPTIONAL_H

#include <stdexcept>


namespace optional {
namespace exp {

struct None {};

template<typename T> struct Some {
    T value;
    Some(T in) : value{in} {}
    Some() {}
};

template<typename T> struct Some<T*> {
    T* value{nullptr};
    Some(T* in) : value{in} {}
    Some() {}
};

template<typename T> class Option {
public:
    Some<T> value;
    bool valid;
    Option(Some<T>&& v) : value{std::forward<Some<T>
                                >(v)}, valid{true} {}
    Option(T v) : value{v}, valid{true} {}
    Option(None) : valid{false} {}
    bool ok() const noexcept { return valid; }
    const T& get() const noexcept {
        if (valid){
            return value.value;
        }
        throw std::runtime_error("terminate, invalid opt");
    }
    template<typename F, typename R = std::result_of_t<F(T)>>
    Option<R> apply(F&& f) const {
        return valid ? Option<R>(f(std::forward<T>(value.value))) : None();
    }
    T get_or(T in) noexcept {
        return valid ? value.value: in;
    }
};

template<typename T> class Option<T*> {
public:
    Some<T*> value;
    Option(Some<T*> v) : value{v} {}
    Option(T* v) : value{v} {}
    Option(None) : value{nullptr} {}
    bool ok() { return value.value; }
    const T* get() const noexcept {
        if (value.value){
            return value.value;
        }
        throw std::runtime_error("terminate, invalid opt");
    }
    template<typename F, typename R = std::result_of_t<F(T)>>
    Option<R> apply(F&& f) const {
        return value.value ? Option<R>(f(value.value)) : None();
    }
    T* get_or(T* in) noexcept {
        return value.value ? value.value : in;
    }
};

template<typename F, typename T> struct Delegate {
    F f;
    Option<T>& opt;
    ~Delegate() noexcept { if (opt.ok()) f(opt); }
};

template<typename F, typename T>
auto scope(Option<T>& opt, F&& f) {
    return Delegate<F, T>{f, opt};
}

} // end of namespace exp
} // end of namespace optional

#endif // EXTRA_OPTIONAL_H
