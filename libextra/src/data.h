#ifndef EXTRA_DATA_H
#define EXTRA_DATA_H

#include "ansi.h"
#include "err.h"

namespace data {

template<typename T> struct WithCred {
    T data;
    ucred cred;
    template<typename U> bool eq_cred(const WithCred<U>& rhs) const {
        return ansi::eq(cred, rhs.cred);
    }
};

template<unsigned len, typename T>
struct Packet {
    static_assert(sizeof(T) <= msg::maxlen, "max socksize exceded");
    static_assert(sizeof(T) > 0, "must be positive");
    void snd(const int fd, const T& msg) {
        err::donotfail_errno("send", send, fd, &msg, sizeof(T), ansi::nosig);
    }
    T rcv(const int fd) {
        T res;
        auto l = sizeof(T);
        err::doreturn_errno("sockopt", setsockopt, fd, ansi::ss, ansi::so::rlow,
                            &l, sizeof(l));
        err::donotfail_errno("recv", ansi::recv, fd, (char*)&res, sizeof(T), 0);
        return res;
    }
    WithCred<T> rcv_cred(const int fd) {
        T data;
        WithCred<T> res{data, ucred{65535, 65535, 65535}};
        err::donotfail_errno("recv", ansi::cred_recv, fd, (char*)&res.data,
                             sizeof(T), &res.cred);
        return res;
    }
};

template<unsigned len>
struct Packet<len, char*> {
    inline void snd(const int fd, const char* cmsg) {
        auto msg = std::string(cmsg);
        Packet<len, std::string>().snd(fd, msg);
    }
};

template<typename T>
struct Packet<0, msg::Empty<T>> {
    WithCred<T> rcv_cred(const int) {
        throw std::runtime_error("zero");
    }
};

template<unsigned len>
struct Packet<len, std::string> {
    static_assert(len <= msg::maxlen, "max socksize exceded");
    void snd(const int fd, const std::string& msg) {
        err::doreturn("message too big", msg.size() > len);
        std::string fullmsg{msg};
        fullmsg.resize(len, '\0');
        err::donotfail_errno("send", send, fd, fullmsg.c_str(), len,
                             ansi::nosig);
    }
    std::string rcv(const int fd) {
        char data[len+1]{0};
        auto l = len;
        err::doreturn_errno("sockopt", setsockopt, fd, ansi::ss, ansi::so::rlow,
                            &l, sizeof(l));
        err::donotfail_errno("recv", recv, fd, data, len, 0);
        return std::string(data);
    }
    WithCred<std::string> rcv_cred(const int fd) {
        char data[len+1]{0};
        auto l = len;
        ucred cred{65535, 65535, 65535};
        err::doreturn_errno("sockopt", setsockopt, fd, ansi::ss, ansi::so::rlow,
                            &l, sizeof(l));
        err::donotfail_errno("recv", ansi::cred_recv, fd, data, len, &cred);
        return WithCred<std::string>{std::string(data), cred};
    }
};

template<unsigned len, typename T>
struct Packet<len, std::vector<T>> {
    static_assert(len*sizeof(T) <= msg::maxlen, "max socksize exceded");
    static_assert(sizeof(T) > 0, "must be positive");
    void snd(const int fd, const std::vector<T>& msg) {
        err::doreturn("message too big", msg.size() > len);
        std::vector<T> fullmsg{msg};
        fullmsg.resize(len, static_cast<T>(0));
        err::donotfail_errno("send", send, fd, &fullmsg[0], len*sizeof(T),
                ansi::nosig);
    }
    std::vector<T> rcv(const int fd) {
        std::vector<T> data(len, 0);
        auto l = len*sizeof(T);
        err::doreturn_errno("sockopt", setsockopt, fd, ansi::ss, ansi::so::rlow,
                            &l, sizeof(l));
        err::donotfail_errno("recv", recv, fd, &data[0], len*sizeof(T), 0);
        return data;
    }
    WithCred<std::vector<T>> rcv_cred(const int fd) {
        WithCred<std::vector<T>> res{
            std::vector<T>(len, 0),
                    ucred{65535, 65535, 65535}
        };
        auto l = len*sizeof(T);
        err::doreturn_errno("sockopt", setsockopt, fd, ansi::ss, ansi::so::rlow,
                            &l, sizeof(l));
        err::donotfail_errno("recv", ansi::cred_recv, fd, &res.data[0],
                             len*sizeof(T), &res.cred);
        return res;
    }
};

} // end of namespace data

#endif // EXTRA_DATA_H
