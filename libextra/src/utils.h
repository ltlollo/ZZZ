#ifndef EXTRA_UTILS_H
#define EXTRA_UTILS_H

#include <iostream>
#include <chrono>
#include "func.h"

namespace fun {

template<typename T, typename U, typename... Ts>
void print_args(T&& padd, U&& arg, Ts&&... args) {
    std::cerr << padd << arg << '\n';
    print_args(std::forward<Ts>(args)...);
}

template<typename T, typename U>
void print_args(T&& padd, U&& arg) {
    std::cerr << padd << arg << '\n';
}

template<typename P, typename... Ts>
inline void functest(P&& f, Ts&&... args) {
    try {
        f(std::forward<Ts>(args)...);
        std::cerr << "returned successfully\n\twith args:\n";
        print_args("\t", std::forward<Ts>(args)...);
    } catch(std::exception& e) {
        std::cerr << "execption caught: " << e.what() << "\n\twith args:\n";
        print_args('\t', std::forward<Ts>(args)...);
    }
}

// consider removal
template<typename T, typename P, typename... Args>
void measure(T&& msg, P&& f, Args&&... args) {
    std::chrono::time_point<std::chrono::high_resolution_clock> start{
        std::chrono::high_resolution_clock::now()};
    f(std::forward<Args>(args)...);
    std::chrono::time_point<std::chrono::high_resolution_clock> end{
        std::chrono::high_resolution_clock::now()};
    std::cerr << msg << " took: "
              << std::chrono::duration_cast<std::chrono::milliseconds>
                 (end - start).count() << "ms\n";
}

struct Bencher {
    const char* msg;
    template<unsigned N>
    Bencher(const char(&msg)[N]) : msg{msg} {}

    template<unsigned N, typename P, typename... Args>
    Bencher(const char(&msg)[N], P&& f, Args&&... args) {
        std::chrono::time_point<std::chrono::high_resolution_clock> start{
            std::chrono::high_resolution_clock::now()};
        f(std::forward<Args>(args)...);
        std::chrono::time_point<std::chrono::high_resolution_clock> end{
            std::chrono::high_resolution_clock::now()};
        std::cerr << msg << " took: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>
                     (end - start).count() << "ms\n";
    }

    template<typename P, typename... Args>
    auto operator()(P&& f, Args&&... args) && {
        std::chrono::time_point<std::chrono::high_resolution_clock> start{
            std::chrono::high_resolution_clock::now()};
        auto res = f(std::forward<Args>(args)...);
        std::chrono::time_point<std::chrono::high_resolution_clock> end{
            std::chrono::high_resolution_clock::now()};
        std::cerr << msg << " took: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>
                     (end - start).count() << "ms\n";
        return res;
    }
};



template<typename T> volatile T& access(T& val) {
    return const_cast<volatile T&>(val);
}
void barrier() {  asm volatile("" : : : "memory"); }

}



#endif // EXTRA_UTILS_H
