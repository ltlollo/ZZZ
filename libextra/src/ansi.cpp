#include "ansi.h"

namespace ansi {

int sun_connect(const int fd, const sockaddr_un* addr) noexcept {
    return connect(fd, (sockaddr*)addr, SUN_LEN(addr));
}

int sun_bind(const int fd, const sockaddr_un* addr) noexcept {
    return bind(fd, (sockaddr*)addr, sizeof(*addr));
}

int signal(const int sig, void(*f)(int, siginfo_t*, void*)) noexcept {
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_sigaction = f;
    act.sa_flags = SA_SIGINFO;
    return sigaction(sig, &act, nullptr);
}

int provide_creds(const int fd) noexcept {
    int op = 1;
    return setsockopt(fd, ss, so::cred, &op, sizeof(op));
}

int cred_recv(const int fd, char* buf, const size_t size,
              ucred* cred) noexcept {
        char control[CMSG_SPACE(sizeof(ucred))] = {0};
        msghdr msg{0,0,0,0,0,0,0};
        cmsghdr* cmsg;
        iovec iov;
        iov.iov_base = buf;
        iov.iov_len = size;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        msg.msg_control = control;
        msg.msg_controllen = sizeof(control);
        int res{-1};
        if (recvmsg(fd, &msg, 0) < 0) {
            return res;
        }
        cmsg = CMSG_FIRSTHDR(&msg);
        while (cmsg != nullptr) {
                if (cmsg->cmsg_level == ansi::ss &&
                    cmsg->cmsg_type  == ansi::scm_cred) {
                        memcpy(cred, CMSG_DATA(cmsg), sizeof(cmsghdr));
                        res = iov.iov_len;
                }
                cmsg = CMSG_NXTHDR(&msg, cmsg);
        }
        return res;
}

int cred_send(const int fd, char* buf, const size_t len) noexcept {
    msghdr msg{0,0,0,0,0,0,0};
    iovec iov;
    iov.iov_base = buf;
    iov.iov_len = len;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    return sendmsg(fd, &msg, nosig);
}

bool eq(const ucred& fst, const ucred& snd) noexcept {
    return (fst.gid == snd.gid) && (fst.pid == snd.pid) && (fst.uid == snd.uid);
}

namespace exp {
bool dfork() {
    pid_t fd{ansi::fork()};
    err::donotfail_errno("fork once", fd);
    if (fd) { return false; }
    fd = ansi::fork();
    err::donotfail_errno("fork twice", fd);
    if (fd) { return false; }
    return true;
}
}

}
