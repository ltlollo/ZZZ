#ifndef EXTRA_FUNC_META_H
#define EXTRA_FUNC_META_H

#include <utility>
#include "futils.h"

namespace fun {

namespace cond {
struct True{};
struct False{};
struct Or{};
struct And{};
struct Else{};
struct Del{};
}

template<bool V> struct Not {
    static constexpr bool value = !V;
};

template<typename T> constexpr bool not_f(...) {
    return v_of<Not<v_of<T>()>>();
}

template<typename NS, typename S, typename... TT>
struct exec_after {
    using type = t_of<exec_after<NS, TT...>>;
    template<typename ns, typename s, typename... tt>
    constexpr type operator()(ns&& v, s, tt&&... ts) {
        return exec_after<ns, tt...>{}(std::forward<ns>(v),
        std::forward<tt>(ts)...);
    }
};

template<typename S, typename F,  typename... TT>
struct exec_after<S, S, F, TT...> {
    using type = std::result_of_t<F(TT...)>;
    template<typename s, typename f, typename... tt>
    constexpr type operator()(s, s, f&& fv, tt&&... ts) {
        return fv(std::forward<tt>(ts)...);
    }
};

template<typename NS, typename S>
struct exec_after<NS, S> {
    using type = void;
    // tag not found, the instance is non callable
};

template<typename NS, typename S, typename... TT>
struct exec_before_h {
    using type = t_of<exec_before_h<NS, TT..., S>>;
    template<typename ns, typename s, typename... tt>
    constexpr auto operator()(ns&& vns, s&& vs, tt&&... ts) {
        return exec_before_h<ns, tt..., s>{}(std::forward<ns>(vns),
        std::forward<tt>(ts)..., std::forward<s>(vs));
    }
};

template<typename S, typename... TT>
struct exec_before_h<S, S, TT...> {
    using type = t_of<exec_after<S, TT...>>;
    template<typename s, typename... tt>
    constexpr type operator()(s&& vs, s, tt&&... ts) {
        return exec_after<s, tt...>{}(std::forward<s>(vs),
        std::forward<tt>(ts)...);
    }
};

template<typename S, typename... TT>
struct exec_before {
    using type = t_of<exec_before_h<S, TT..., S>>;
    template<typename s, typename... tt>
    constexpr type operator()(s vs, tt&&... ts) {
        return exec_before_h<s, tt..., s>{}(std::forward<s>(vs),
        std::forward<tt>(ts)..., std::forward<s>(vs));
    }
};

template<typename T, typename U, typename... TT>
struct contains {
    static constexpr bool value = v_of<contains<T, TT...>>();
};

template<typename T, typename... TT>
struct contains<T, T, TT...> {
    static constexpr bool value = true;
};

template<typename T, typename U>
struct contains<T, U> {
    static constexpr bool value = false;
};

template<typename T> struct If {};

template<> struct If<cond::True> {
    template<typename... TT>
    constexpr std::enable_if_t<v_of<contains<cond::Else, TT...>>(),
    t_of<exec_before<cond::Else, TT...>>> operator()(TT&&... Ts) {
        return exec_before<cond::Else, TT...>{}(cond::Else(),
        std::forward<TT>(Ts)...);
    }
    template<typename F, typename... TT>
    constexpr std::enable_if_t<not_f<contains<cond::Else, F, TT...>>(),
    std::result_of_t<F(TT...)>> operator()(F&& f, TT&&... Ts) {
        return f(std::forward<TT>(Ts)...);
    }
};

template<> struct If<cond::False> {
    template<typename... TT>
    constexpr std::enable_if_t<v_of<contains<cond::Else, TT...>>(),
    t_of<exec_after<cond::Else, TT...>>> operator()(TT&&... Ts) {
        return exec_after<cond::Else, TT...>{}(cond::Else(),
        std::forward<TT>(Ts)...);
    }
};

template<typename T, typename... TT>
constexpr auto if_f(T, TT&&... Ts) {
    return If<T>{}(std::forward<TT>(Ts)...);
}
}

#endif // EXTRA_FUNC_META_H
