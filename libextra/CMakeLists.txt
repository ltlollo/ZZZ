cmake_minimum_required(VERSION 3.0)

project(libextra)

set(VERSION_MAJOR "0")
set(VERSION_MINOR "5")
set(VERSION_PATCH "0")

add_subdirectory(src)

include(DebPack.cmake)
